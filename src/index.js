
import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Home from './views/Home';
import Register from './views/Register';
import Login from './views/Login';
import mailVerification from './views/mailVerification';
import verificationCode from './views/verificationCode';
import verification from './views/verification';

import '../src/services/user.services';
import Profile from './views/profile';
import createProfile from './views/createProfile';
import updateProfile from './views/updateProfile';
import Video from './views/video';
import createVideo from './views/createVideo';
import updateVideo from './views/updateVideo';
import loginClient from './views/loginClient';
import videoPlayer from './views/videoPlayer';
render(
  <Router>
    <div>
      <Route exact path="/" component={Login} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/register" component={Register} />
      <Route exact path="/home" component={Home} />
      <Route exact path="/mailVerification" component={mailVerification} />
      <Route exact path="/verificationCode" component={verificationCode} />
      <Route exact path="/verification" component={verification} />
      <Route exact path="/profile" component={Profile} />
      <Route exact path="/video" component={Video} />
      <Route exact path="/createProfile" component={createProfile} />
      <Route exact path="/updateProfile" component={updateProfile} />

      <Route exact path="/createVideo" component={createVideo} />
      <Route exact path="/updateVideo" component={updateVideo} />
     
      <Route exact path="/loginC" component={loginClient} />
      <Route exact path="/videoPlayer" component={videoPlayer} />
    </div>
  </Router>, document.getElementById('root')
);