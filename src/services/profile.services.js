import axios from 'axios';
import Global from '../global.js';

export default {
    getProfile() {
        var data = {
            id_user: localStorage.getItem("id_user")
        }
        return axios.post(Global.API_PROFILE + "/get", data)
            .then(function (response) {
                return response;
            })
            .catch(function (error) {
                return error;
            });

    },
    postProfile(data) {
        return axios.post(Global.API_PROFILE, data)
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    },
    getProfileId(Id) {
        return axios.get(Global.API_PROFILE + '/' + Id)
            .then(function (response) {
                return response;
            })
            .catch(function (error) {
                return error;
            });

    },
    updateProfileId(Id, data) {
        return axios.patch(Global.API_PROFILE + '/' + Id, data)
            .then(function (response) {

                return response;
            })
            .catch(function (error) {
                console.log(data);
                return error;
            });
    },
    deleteProfileId(Id){
        return axios.delete(Global.API_PROFILE +'/'+ Id)
        .then(function(response){
         
          return response;
        })
        .catch(function(error){
         
          return error;
        });
      },
      login(data){
        return axios.post(Global.API+'loginC', data)
        .then(function(response){
            return response;
          })
          .catch(function(error){
            return error;
          });
        },
}

