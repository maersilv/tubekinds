
import axios from 'axios';
import Global from '../global.js';

export default {
    getUser() {
        return axios.get(Global.API_USER)
            .then(res => {
                return res.data;
            });
    },
    postUser(data) {
        return axios.post(Global.API_USER, data)
          .then(function (response) {
            console.log(response);
          })
          .catch(function (error) {
            console.log(error);
          });
    },
    login(data){
      return axios.post(Global.API+"login", data)
          .then(function (response) {
            return response;
          })
          .catch(function (error) {
            console.log(data);
          });
    },

    verification(data){
      return axios.post(Global.API+"authentication", data)
          .then(function (response) {
            return response;
          })
          .catch(function (error) {
            console.log(data);
          });
    },
    accessCode(data){
      return axios.post(Global.API+"code", data)
          .then(function (response) {
            return response;
          })
          .catch(function (error) {
            console.log(data);
          });
    },

    confirmToken(token){
      return axios.get(Global.API + "token_confirm",{ headers: {"Authorization" : `Bearer ${token}`} })
      .then(function(response){
        return true;
      })
      .catch(function(error){
        return false;
      });
    },
   
 
}

