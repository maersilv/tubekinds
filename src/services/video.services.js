
import axios from 'axios';
import Global from '../global.js';

export default {
  getVideo() {
    var data = {
      id_user: localStorage.getItem("id_user")
    }
    return axios.post(Global.API_VIDEO + "/get", data)
      .then(function (response) {
        return response;
      })
      .catch(function (error) {
        return error;
      });


  },


  postVideo(data) {
    return axios.post(Global.API_VIDEO, data)
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  },

  getVideoId(Id) {
    return axios.get(Global.API_VIDEO + '/' + Id)
      .then(function (response) {
        return response;
      })
      .catch(function (error) {
        return error;
      });

  },
  updateVideoId(Id, data) {
    return axios.patch(Global.API_VIDEO + '/' + Id, data)
      .then(function (response) {
        console.log(response);
        return response;
      })
      .catch(function (error) {
        console.log(data);
        return error;
      });
  },
  deleteVideoId(Id) {
    return axios.delete(Global.API_VIDEO + '/' + Id)
      .then(function (response) {
        
        return response;
      })
      .catch(function (error) {
       
        return error;
      });
  },
  filter(data){
    return axios.post(Global.API_VIDEO+'/search',data)
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      return error;
    });
  }
}
