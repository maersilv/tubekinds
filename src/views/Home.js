import React, { Component } from 'react';
import '../App.css';
import User from '../services/user.services';

class Home extends Component {
  //token
  constructor() {
    super();

    this.state = {
      token: false
    }
  }


  async componentDidMount() {
    
    this.setState({
      token: await User.confirmToken(localStorage.getItem('token_Tubekids'))
    });
   
    if (!localStorage.getItem('token_Tubekids')) {
      console.log(localStorage.getItem('token_Tubekids'));
      if (this.state.token) {

        window.location.href = '/login';
      }
    }
    
  }

  url = () => {

    this.props.history.push({
      pathname: '/profile',
      state: {
        profiles: {
          id: "1",
          nombre: "nombre"
        }
      }
    });
  }
  video = () => {

    this.props.history.push({
      pathname: '/video',
      state: {
        profiles: {
          id: "1",
          nombre: "nombre"
        }
      }
    });

  }

  logout = () => {
    localStorage.clear();
    window.location.href = '/login';
  }

  render() {
        return (
          <nav class="navbar navbar-expand-lg navbar-light bg-light" >
            <a class="navbar-brand" href="#">TubeKids</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                  <a type="button" class="nav-link" onClick={this.url} >Perfiles </a>
                </li>
                <li class="nav-item">
                  <a type="button" class="nav-link" onClick={this.video}>Videos</a>
                </li>



              </ul>


              <button type="button" class="btn btn-info" onClick={this.logout}>Cerrar sesion</button>




            </div>

          </nav>
        );
    



  }
}

export default Home;
