import React, { Component } from 'react';

import '../App.css';
import '../css/Login.css';
import User from '../services/user.services';
import Global from  '../global';
import { isCompositeComponent } from 'react-dom/test-utils';



class Login extends Component {

    constructor() {
        super();

        this.state = {
            formControls: {
                email: '',
                password: ''
            }
        }
    }

    changeHandler = event => {
        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };
        const updatedFormElement = {
            ...updatedControls[name]
        };
        updatedFormElement.value = value;
        updatedFormElement.touched = true;

        updatedControls[name] = updatedFormElement;

        this.setState({
            formControls: updatedControls
        });
    }


    formSubmitHandler = () => {
        if (this.state.formControls.email == '') {
            alert("Campo email requerido ");
        } else {
            if (this.state.formControls.password == '') {
                alert("Campo contraseña requerido");
            } else {
                User.login(this.state.formControls).then(data => {
                    if (typeof data != 'undefined') {
                        if (data.data[0].state) {
                            if (data.state = true) {
                               
                                Global.EMAIL = data.data[0].email;
                                this.props.history.push('/verificationCode');
                            }
                        } else {
                            alert('Por favor active la cuenta');
                            this.setState({
                                formControls: {
                                    email: { value: "" },
                                    password: { value: "" },
                                }
                            });
                        }


                    } else {
                        alert('Usuario o Contraseña Invalidos');
                        this.setState({
                            formControls: {
                                email: { value: "" },
                                password: { value: "" },
                            }
                        });
                        return false;
                    }
                });
            }
        }
    }




    render() {
        return (
            <section class="login-block">
                <div class="container">
                    <div class="row ">
                        <div class="col login-sec">
                            <h2 class="text-center">Login</h2>

                            <div class="login-form">
                                <div class="form-group">
                                    <label>Correo</label>
                                    <input
                                        type="email"
                                        class="form-control"
                                        name="email"
                                        id="email"
                                        placeholder="Digite su correo"
                                        value={this.state.formControls.email.value}
                                        onChange={this.changeHandler}
                                        required />
                                </div>

                                <div class="form-group">
                                    <label>Contraseña</label>
                                    <input type="password" class="form-control" name="password" id="password" required min="8"
                                        placeholder="Digite su contraseña" required value={this.state.formControls.password.value}
                                        onChange={this.changeHandler} />
                                </div>

                                <button type="submit" class="btn btn-login float-right" onClick={this.formSubmitHandler}>Sign in</button>
                                <label>No tienes cuenta?</label>
                                <br />
                                <a href="Register">Crea una</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        );
    }
}



export default Login;

