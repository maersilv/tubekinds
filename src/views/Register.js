import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { Router } from "react-router-dom";
import '../App.css';
import '../css/Register.css';
import User from '../services/user.services';
import '../views/mailVerification';


class Register extends Component {

    constructor() {
        super();

        this.state = {
            formControls: {
                name: '',
                lastname: '',
                email: '',
                country: '',
                cellphone: '',
                date_of_birth: '',
                password: '',
                con_password: '',
            }
        }
    }

    changeHandler = event => {
        const name = event.target.name;
        const value = event.target.value;


        var updatedControls = {
            ...this.state.formControls
        };
        updatedControls[name] = value;

        this.setState({
            formControls: updatedControls
        });
    }
validateEmail(){
    if(this.state.formControls.email==this.state.email){
        alert("El email que ingreso ya esta en uso!");
    }
}
    formSubmitHandler = () => {
        
        if (this.state.formControls.name == '') {
            alert("Campo nombre requerido");
        } else {
            if (this.state.formControls.lastname == '') {
                alert("Campo apellido requerido");
            } else {
                if (this.state.formControls.email == '') {
                    alert("Campo email requerido ");
                } else {
                    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.state.formControls.email) == false) {
                        alert("¡Has introducido una dirección de correo electrónico no válida!");
                    } else {
                        if (this.state.formControls.cellphone == '') {
                            alert("Campo telefono requerido");
                        } else {
                            if (this.state.formControls.date_of_birth == '') {
                                alert("Campo fecha requerido");
                            } else {
                                var año = this.state.formControls.date_of_birth;
                                año = parseInt(año);
                                var edad = 2019 - año;
                                if (edad <= 18) {
                                    alert("Usted no es mayor de edad");
                                    document.getElementById('date').value = "";

                                }
                                if (this.state.formControls.password == '' || this.state.formControls.con_password == '') {
                                    alert("Campos de contraseña son requeridos");

                                } else if (this.state.formControls.password != this.state.formControls.con_password) {
                                    alert("Las contraseñas no coinciden");
                                    var form = this.state.formControls;
                                    form.password = '';
                                    form.con_password = '';
                                    this.setState({
                                        formControls: form
                                    });
                                } else {
                                    User.postUser(this.state.formControls).then(data => {
                                       this.props.history.push('/mailVerification');
                                    });
                                }
                            }
                        }
                    }
                }
            }
        }
    }




    render() {
        return (
             <center>
            <section class="testbox">
                
                <div id="formRegister">
                <h1>Registro</h1>
                    <input type="text" name="name" id="name" placeholder="Nombre" required value={this.state.formControls.name} onChange={this.changeHandler}/>

                    <input type="text" name="lastname" id="lastName" placeholder="Apellido" required value={this.state.formControls.lastname} onChange={this.changeHandler}/>

                    <input type="text" unique='true' name="email" id="email" placeholder="Email" required value={this.state.formControls.email} onChange={this.changeHandler}/>

                    <input type="text" name="country" id="country" placeholder="Pais" value={this.state.formControls.country} onChange={this.changeHandler}/>

                    <input type="tel" name="cellphone" id="tel" placeholder="Número telefónico" required min="8" max="8" value={this.state.formControls.cellphone} onChange={this.changeHandler} />

                    <input type="date" name="date_of_birth" id="date" placeholder="Fecha de nacimiento" required value={this.state.formControls.date_of_birth} onChange={this.changeHandler} />

                    <input type="password" name="password" id="password" placeholder="Contraseña" required value={this.state.formControls.password} onChange={this.changeHandler} min="8" />

                    <input type="password" name="con_password" id="con_pass" placeholder="Confirmar contraseña" required value={this.state.formControls.con_password} onChange={this.changeHandler} min="8" />
                    <br />
                    <br />
                   
                    <button type="submit" class="btn btn-info" onClick={this.formSubmitHandler}>Registrar</button>
                    <br />
                    <label> ¿Ya tienes una cuenta?</label>
                    <br />
                    <a href="login">Iniciar Sesion</a>
                  


                </div>

            </section>

            </center>

        );
    }
}



export default Register;

