import React, { Component } from 'react';
import '../App.css';
import '../css/createProfile.css';
import User from '../services/user.services';
import ProfileS from '../services/profile.services';


class createProfile extends Component {
    //token
    state = {
        token: false
    }
    constructor() {
        super();

        this.state = {

            formControls: {
                id_user: localStorage.getItem("id_user"),
                name: { value: "" },
                username: { value: "" },
                pin: { value: "" },
                age: { value: "" },
            }

        }
    }

    componentDidMount(){
        this.token();
    }

    async token() {
        this.setState({
            token: await User.confirmToken(localStorage.getItem('token_Tubekids'))
        });

        if (!localStorage.getItem('token_Tubekids')) {

            if (this.state.token) {
                window.location.href = '/login';
            }
        }
    }






    changeHandler = event => {
        const name = event.target.name;
        const value = event.target.value;


        var updatedControls = {
            ...this.state.formControls
        };
        updatedControls[name] = value;

        this.setState({
            formControls: updatedControls
        });
    }

    formSubmitHandler = () => {
        if (this.state.formControls.name.value == '') {
            alert("Campo nombre requerido");
        } else {
            if (this.state.formControls.username.value == '') {
                alert("Campo usuario requerido");
            } else {
                if (this.state.formControls.pin.value == '') {
                    alert("Campo pin requerido");
                } else {
                    if (this.state.formControls.age.value == '') {
                        alert("Campo edad es  requerido");
                    } else {
                        ProfileS.postProfile(this.state.formControls).then(data => {
                            alert("Se creo el perfil con exito");
                            
                            this.setState({
                                formControls: {

                                    name: { value: "" },
                                    username: { value: "" },
                                    pin: { value: "" },
                                    age: { value: "" },


                                }
                            });

                        });
                    }
                }
            }
        }
    }



    render() {
        return (
            <div class="container">
                <div class="row">


                    <center>
                        <h1>Create Profile</h1>
                    </center>
                    <hr />

                    <input type="text" id="name" name="name" placeholder="Name" required value={this.state.formControls.name.value} onChange={this.changeHandler} />
                    <input type="text" id="username" name="username" placeholder="Username" required value={this.state.formControls.username.value} onChange={this.changeHandler} />
                    <input type="number" id="pin" name="pin" placeholder="Pin" required value={this.state.formControls.pin.value} onChange={this.changeHandler} min="6" />
                    <input type="number" id="age" name="age" placeholder="Age" value={this.state.formControls.age.value} onChange={this.changeHandler} />
                    <hr />
                    <button type="submit" name="submit" class="btn btn-info btn-block" onClick={this.formSubmitHandler}>Create</button>

                </div>
                <a class='flotante' href='/profile' ><img src="https://cdn3.iconfinder.com/data/icons/essential-rounded/66/Rounded-13-512.png" width="40" height="41" border="0" /></a>

            </div>



        );

    }
}

export default createProfile;