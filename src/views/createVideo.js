import React, { Component } from 'react';
import '../App.css';
import User from '../services/user.services';
import VideoS from '../services/video.services';

class createVideo extends Component {
  state = {
    token: false,
  }
  constructor() {
    super();

    this.state = {

      formControls: {
        id_user: localStorage.getItem("id_user"),
        name: '',
        url: ''
      }

    }
  }



  async token() {

    this.setState({
      token: await User.confirmToken(localStorage.getItem('token_Tubekids'))
    });

    if (!localStorage.getItem('token_Tubekids')) {

      if (this.state.token) {
        window.location.href = '/login';
      }
    }
  }

  changeHandler = event => {
    const name = event.target.name;
    const value = event.target.value;


    var updatedControls = {
      ...this.state.formControls
    };
    updatedControls[name] = value;

    this.setState({
      formControls: updatedControls
    });
  }
  formSubmitHandler = () => {
    if (this.state.formControls.name == '') {
      alert("Campo nombre requerido");
    } else {
      if (this.state.formControls.url == '') {
        alert("Campo url requerido");
      }
      VideoS.postVideo(this.state.formControls).then(data => {

        alert("Se creo el video con exito");
        this.setState({
          formControls: {
            name: { value: "" },
            url: { value: "" },

          }
        });

      });
    }
  }



  async componentDidMount() {
    this.setState({
      token: await User.confirmToken(localStorage.getItem('token_Tubekids'))
    });

    if (localStorage.getItem('token_Tubekids')) {
      if (!this.state.token) {
        window.location.href = '/login';
      }
    }
  }
  render() {
    return (


      //form
      <div class="container">
        <div class="row">


          <center>
            <h1>Create Video</h1>

          </center>

          <input type="text" id="name" name="name" placeholder="Name" required value={this.state.formControls.name.value} onChange={this.changeHandler} />
          <input type="text" id="url" name="url" placeholder="Url" required value={this.state.formControls.url.value} onChange={this.changeHandler} />

          <button type="submit" name="submit" class="btn btn-info btn-block" onClick={this.formSubmitHandler}>Create</button>
          <a class='flotante' href='/video' ><img src="https://cdn3.iconfinder.com/data/icons/essential-rounded/66/Rounded-13-512.png" width="40" height="41" border="0" /></a>

        </div>
      </div>


    );

  }
}

export default createVideo;