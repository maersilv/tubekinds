import React, { Component } from 'react';
import '../css/LoginC.css';
import Profile from '../services/profile.services';
import Global from '../global';
import { isCompositeComponent } from 'react-dom/test-utils';



class loginClient extends Component {

    constructor() {
        super();

        this.state = {
            formControls: {
                username: '',
                pin: ''
            }
        }
    }

    changeHandler = event => {
        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };
        const updatedFormElement = {
            ...updatedControls[name]
        };
        updatedFormElement.value = value;
        updatedFormElement.touched = true;

        updatedControls[name] = updatedFormElement;

        this.setState({
            formControls: updatedControls
        });
    }


    formSubmitHandler = () => {
        if (this.state.formControls.username.value == '') {
            alert("Campo usuario requerido ");
        } else {
            if (this.state.formControls.pin.value == '') {
                alert("Campo pin requerido");
            } else {

                Profile.login(this.state.formControls).then(data => {
                   if(data.status == 200){
                    localStorage.setItem('token_Tubekids', data.data.token);
                    localStorage.setItem('id_user', data.data.id_user);
                    this.props.history.push('/videoPlayer');
                   }else{
                    alert("Usuario o pin invalidos");
                   }
                   
                }).catch((err) => {
                    console.log(err);
                  });
            }
        }
    }




    render() {
        return (
            <section class="login-block">
                <div class="container">
                    <div class="row ">
                        <div class="col login-sec">
                            <h2 class="text-center">Login de Niños</h2>

                            <div class="login-form">
                                <div class="form-group">
                                    <label>Usuario</label>
                                    <input
                                        type="text"
                                        class="form-control"
                                        name="username"
                                        id="username"
                                        placeholder="Digite su usuario"
                                        value={this.state.formControls.username.value}
                                        onChange={this.changeHandler}
                                        required />
                                </div>

                                <div class="form-group">
                                    <label>Pin</label>
                                    <input type="password" class="form-control" name="pin" id="pin" required min="8"
                                        placeholder="Digite su pin" required value={this.state.formControls.pin.value}
                                        onChange={this.changeHandler} />
                                </div>

                                <button type="submit" class="btn btn-login float-right" onClick={this.formSubmitHandler}>Sign in</button>

                            </div>
                        </div>
                    </div>
                </div>
            </section>

        );
    }
}



export default loginClient;

