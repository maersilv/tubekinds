import React, { Component } from 'react';
import '../css/profile.css';
import User from '../services/user.services';
import ProfileS from '../services/profile.services';



class Profile extends Component {

  //token
  state = {
    token: false,
    Profile: []
  };
  constructor() {
    super();

  }


  async componentDidMount() {
    this.token();
    ProfileS.getProfile().then(res => {
      const Profile = res.data;
      this.setState({ Profile });
      
    });

  }

  async token() {

    this.setState({
      token: await User.confirmToken(localStorage.getItem('token_Tubekids'))
    });

    if (!localStorage.getItem('token_Tubekids')) {

      if (this.state.token) {
        window.location.href = '/login';
      }
    }
  }
  sendIdProfile(e) {
    e.preventDefault();
    localStorage.setItem('id_profile', e.target.id);
    window.location.href = '/updateProfile';
   
  }

  deleteIdProfile=(e)=>{
    
   
    ProfileS.deleteProfileId(e.target.id).then(res => {
      window.location.href = '/profile';
  
    })
  
  
  }
  url = () => {

    this.props.history.push('/profile');
  }
  video = () => {

    this.props.history.push('/video');
  }
  logout = () => {
    localStorage.clear();
    window.location.href = '/login';
  }
  render() {

    return (


      < div>
        <nav class="navbar navbar-expand-lg navbar-light bg-light" >
          <a class="navbar-brand" href="#">TubeKids</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                <a type="button" class="nav-link" onClick={this.url} >Perfiles </a>
              </li>
              <li class="nav-item">
                <a type="button" class="nav-link" onClick={this.video}>Videos</a>
              </li>



            </ul>
            <a type="button" class="btn btn-info" href="/createProfile">Create</a>

            <button type="button" class="btn btn-info" onClick={this.logout}>Cerrar sesion</button>




          </div>

        </nav>
        <center>
          <table class="table table-hover">
            <thead>
              <tr>
                <th scope="col">Id</th>
                <th scope="col">Name</th>
                <th scope="col">Username</th>
                <th scope="col">Update</th>
                <th scope="col">Delete</th>
              </tr>
            </thead>
            <tbody>
              {this.state.Profile.map(profi =>

                <tr>
                  <td>{profi._id}</td>
                  <td>{profi.name}</td>
                  <td>{profi.username}</td>
                  <td><button id={profi._id} class="btn btn-success"  onClick={this.sendIdProfile} >Update</button></td>
                  <td><button id={profi._id} class="btn btn-danger" onClick={this.deleteIdProfile}>Delete</button></td>



                </tr>

              )}

            </tbody>
          </table>
        </center>

      </div>
    );

  }

}

export default Profile;