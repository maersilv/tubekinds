import React, { Component } from 'react';
import '../App.css';
import '../css/createProfile.css';
import User from '../services/user.services';
import ProfileS from '../services/profile.services';

class updateProfile extends Component {

  //token
  state = {
    token: false
  }

  Profile = new Array();

  constructor() {
    super();

    this.state = {
      formControls: {
        name:"",
        username: "",
        pin: "",
        age: "",
      }
    }
  }
  handleChange = event => {
    const name = event.target.name;
    const value = event.target.value;


    var updatedControls = {
      ...this.state.formControls
    };
    updatedControls[name] = value;

    this.setState({
      formControls: updatedControls
    });
  }

  componentDidMount() {

    this.token();


    let id = localStorage.getItem("id_profile");

    ProfileS.getProfileId(id).then(res => {
      const v = res.data;
      console.log(v);
      this.setState({
        formControls: {
          name:  v.name ,
          username: v.username,
          pin:  v.pin ,
          age: v.age,

        }
      });

    });
  }
  updateProfile = () => {

    let id = localStorage.getItem("id_profile");
  

    ProfileS.updateProfileId(id,this.state.formControls).then(res => {
      alert("Se Actualizo con exito");
      const v = res.data;
      
      this.setState({
        formControls: {
          name:"",
          username: "",
          pin: "",
          age: ""
        }
      });

    })

  }
  async token() {

    this.setState({
      token: await User.confirmToken(localStorage.getItem('token_Tubekids'))
    });

    if (!localStorage.getItem('token_Tubekids')) {

      if (this.state.token) {
        window.location.href = '/login';
      }
    }
  }

  render() {
    return (


      //form
      <div class="container">
        <div class="row">
          <div class="span4 offset4 well">

            <center>
              <legend>Update User</legend>
            </center>
            <form >
              <input type="text" id="name" class="span4" name="name" placeholder="Name complete" value={this.state.formControls.name} onChange={this.handleChange} required />
              <input type="text" id="username" class="span4" name="username" placeholder="Username" value={this.state.formControls.username} onChange={this.handleChange} required />
              <input type="text" id="pin" class="span4" name="pin" placeholder="pin" value={this.state.formControls.pin} onChange={this.handleChange} required />
              <input type="text" id="age" class="span4" name="age" placeholder="Age" value={this.state.formControls.age} onChange={this.handleChange} required />
              <button type="submit" onClick={this.updateProfile} class="btn btn-success btn-block">Update</button>
            </form>
          </div>
        </div>

        <a class='flotante' href='/profile' ><img src="https://cdn3.iconfinder.com/data/icons/essential-rounded/66/Rounded-13-512.png" width="40" height="41" border="0" /></a>
      </div>


    );

  }
}

export default updateProfile;