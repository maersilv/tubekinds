import React, { Component } from 'react';
import User from '../services/user.services';
import VideoS from '../services/video.services';
import '../css/updateVideo.css';

class updateVideo extends Component {


  Video = new Array();
  constructor() {
    super();
    this.state = {
      formControls: {
        name: "",
        url: ""

      },
      token: false,
    }
  }

  handleChange = event => {
    const name = event.target.name;
    const value = event.target.value;


    var updatedControls = {
      ...this.state.formControls
    };
    updatedControls[name] = value;

    this.setState({
      formControls: updatedControls
    });
  }


  componentDidMount() {

    this.token();


    let id = localStorage.getItem("id_video");

    VideoS.getVideoId(id).then(res => {
      const v = res.data;
      this.setState({
        formControls: {
          name: v.name,
          url: v.url
        }
      });
    });
  }

  updateVideo = () => {

    let id = localStorage.getItem("id_video");
  

    VideoS.updateVideoId(id,this.state.formControls).then(res => {
      alert("Se Actualizo con exito");
      const v = res.data;
     
      this.setState({
        formControls: {
          name: "",
          url: ""
        }
      });

    })

  }
  async token() {

    this.setState({
      token: await User.confirmToken(localStorage.getItem('token_Tubekids'))
    });

    if (!localStorage.getItem('token_Tubekids')) {

      if (this.state.token) {
        window.location.href = '/login';
      }
    }
  }

  render() {
    return (

      <div class="container">
        <div class="row">
          <div class="span4 offset4 well">

            <center>
              <legend>Update Video</legend>
            </center>

           

              <input type="text" id="name" name="name" placeholder="Name" value={this.state.formControls.name} onChange={this.handleChange} />

              <input type="text" id="url" name="url" placeholder="Url" value={this.state.formControls.url} onChange={this.handleChange} />

              <button type="submit" onClick={this.updateVideo} class="btn btn-info btn-block">Update</button>

           



          </div>
        </div>

        <a class='flotante' href='/video' ><img src="https://cdn3.iconfinder.com/data/icons/essential-rounded/66/Rounded-13-512.png" width="40" height="41" border="0" /></a>
      </div>


    );

  }



}

export default updateVideo;