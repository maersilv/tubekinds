import React, { Component } from 'react';
import User from '../services/user.services';
import queryString from 'query-string';

class verification extends Component {

    constructor() {
        super();

        this.state = {
            formControls: {
                token: null
            }
        }
    }

    componentDidMount() {
        const values = new queryString.parse(this.props.location.search)
        return values.token;
    }

    sendToken(){
        this.state.formControls.token = this.componentDidMount();
        User.verification(this.state.formControls).then(data => {
            console.log(data);
            this.props.history.push('/login');
        });
    }

    render() {
        return (
        <p>{ this.sendToken() }</p>
        );
    }
}



export default verification;
