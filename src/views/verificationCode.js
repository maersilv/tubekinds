import React, { Component } from 'react';
import '../App.css';
import '../css/verificationCode.css';
import Global from '../global';
import User from '../services/user.services';



class verificationCode extends Component {
    state = {
        token: false,
       
      }
    constructor() {
        super();

        this.state = {
            formControls: {
                email: {value: Global.EMAIL,},
                codeSms: {value: ""}
            }
        }
    }
    async token() {  

        this.setState({
          token: await User.confirmToken(localStorage.getItem('token_Tubekids'))
        });
    
        if (!localStorage.getItem('token_Tubekids')) {
    
          if (this.state.token) {
            window.location.href = '/login';
          }
        }
      }
    changeHandler = event => {
        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };
        const updatedFormElement = {
            ...updatedControls[name]
        };
        updatedFormElement.value = value;
        updatedFormElement.touched = true;

        updatedControls[name] = updatedFormElement;

        this.setState({
            formControls: updatedControls
        });
    }

    formSubmitHandler = () => {
        if (this.state.formControls.codeSms.value == '') {
            alert("Campo de verificacion es requerido");
        } else {
            User.accessCode(this.state.formControls).then(data => {
                if (data.data.state) {


                    localStorage.setItem('token_Tubekids', data.data.token);
                     localStorage.setItem('id_user', data.data.id_user);
                    
                    this.props.history.push('/home');
                } else {
                    alert("Codigo de verificacion erroneo");
                }
            }).catch(
                err => {
                   console.log( err.message);
                }
            );
        }
    }
    render() {
        return (
           
            <div class="wrapper fadeInDown">
                <div id="formContent">

                    <h2 class="active"> Digite su codigo </h2>
                    <input type="text" id="code" class="fadeIn third" name="codeSms" placeholder="Codigo" value={this.state.formControls.codeSms.value}
                                        onChange={this.changeHandler}/>
                        <input type="submit" class="fadeIn fourth" value="Verificar" onClick={this.formSubmitHandler}/>
                </div>
            </div>
         
        );
    }
}
export default verificationCode;