import React, { Component } from 'react';
import '../css/video.css';
import User from '../services/user.services';
import VideoS from '../services/video.services';
import { Redirect } from 'react-router-dom';
import ReactPlayer from 'react-player'

class Video extends Component {
  //token
  state = {
    token: false,
    Video: [],
    filter: {
      text:""
  }

  }

  constructor() {
    super();
  }

  async componentDidMount() {
    this.token();
    VideoS.getVideo().then(res => {
      const Video = res.data;
      this.setState({ Video });


    });

  }
  async token() {

    this.setState({
      token: await User.confirmToken(localStorage.getItem('token_Tubekids'))
    });

    if (!localStorage.getItem('token_Tubekids')) {

      if (this.state.token) {
        window.location.href = '/login';
      }
    }
  }

  sendIdVideo(e) {
    e.preventDefault();
    localStorage.setItem('id_video', e.target.id);
    window.location.href = '/updateVideo';

  }

  deleteIdVideo = (e) => {
    VideoS.deleteVideoId(e.target.id).then(res => {
      alert("Se elimino con exito!")
      window.location.href = '/video';

    })
  }


  url = () => {

    this.props.history.push('/profile');
  }
  video = () => {

    this.props.history.push('/video');
  }
  logout = () => {
    localStorage.clear();
    window.location.href = '/login';
  }
  changeHandler = event => {
      const name = event.target.name;
      const value = event.target.value;


      var updatedControls = {
          ...this.state.filter
      };
      updatedControls[name] = value;

      this.setState({
          filter: updatedControls
      });
  }
  search = () => {
    VideoS.filter(this.state.filter).then(res => {

      const Video = res.data;
      this.setState({ Video });

    });
  }
  render() {
    return (

      < div>
        <nav class="navbar navbar-expand-lg navbar-light bg-light" >
          <a class="navbar-brand" href="#">TubeKids</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                <a type="button" class="nav-link" onClick={this.url} >Perfiles </a>
              </li>
              <li class="nav-item">
                <a type="button" class="nav-link" onClick={this.video}>Videos</a>
              </li>



            </ul>
            <form class="form-inline my-2 my-lg-0">


              <input type="text" class="form-control" id="search" name="text" placeholder="Search..." required value={this.state.filter.text} onChange={this.changeHandler} />
              <button class="btn btn-outline-success my-2 my-sm-0" type="button" onClick={this.search}>Buscar</button>
            </form>
            <a type="button" class="btn btn-info" href="/createVideo">Create</a>
            <button type="button" class="btn btn-info" onClick={this.logout}>Cerrar sesion</button>




          </div>

        </nav>
        <center>

          <table class="table table-hover">
            <thead>
              <tr>
                <th scope="col">Id</th>
                <th scope="col">Name</th>
                <th scope="col">Url</th>

              </tr>
            </thead>

            <tbody>
              {this.state.Video.map(video =>
                <tr>
                  <td>{video._id} </td>
                  <td>{video.name} </td>
                  <td>
                    <ReactPlayer
                      url={video.url}
                      className='react-player'
                      width='90%'
                      height='100%'

                    />

                    <td><button id={video._id} class="btn btn-success" onClick={this.sendIdVideo} >Update</button></td>


                    <td><button id={video._id} class="btn btn-danger" onClick={this.deleteIdVideo}>Delete</button></td>
                  </td>

                </tr>
              )}
            </tbody>

          </table>

        </center>
      </div>
    );

  }


}




export default Video;