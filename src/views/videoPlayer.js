import React, { Component } from 'react';
import '../css/videoPlayer.css';
import User from '../services/user.services';
import VideoS from '../services/video.services';
import ReactPlayer from 'react-player'

class videoPlayer extends Component {


    state = {
        token: false,
        Video: [],
        filter: {
            text:""
        }


    }

    //token
    constructor() {
        super();
    }


    logout = () => {
        localStorage.clear();
        window.location.href = '/loginC';
    }


    async componentDidMount() {
        this.token();
        VideoS.getVideo().then(res => {
            const Video = res.data;
            this.setState({ Video });


        });

    }
    async token() {

        this.setState({
            token: await User.confirmToken(localStorage.getItem('token_Tubekids'))
        });

        if (!localStorage.getItem('token_Tubekids')) {

            if (this.state.token) {
                window.location.href = '/login';
            }
        }


    }

   

    changeHandler = event => {
        const name = event.target.name;
        const value = event.target.value;


        var updatedControls = {
            ...this.state.filter
        };
        updatedControls[name] = value;

        this.setState({
            filter: updatedControls
        });
    }
    search = () =>{
        VideoS.filter(this.state.filter).then(res => {

            const Video = res.data;
            this.setState({ Video });
    
          });
    }
 
    render() {
        return (

            < div>
                <nav class="navbar navbar-expand-lg navbar-light bg-light" >
                    <a class="navbar-brand" href="#">TubeKids</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">

                            <li class="nav-item">
                                <a type="button" class="nav-link" >PlayList</a>
                            </li>



                        </ul>
                        <form class="form-inline my-2 my-lg-0">

                           
                            <input type="text" class="form-control" id="search" name="text" placeholder="Search..." required value={this.state.filter.text} onChange={this.changeHandler} />
                            <button class="btn btn-outline-success my-2 my-sm-0" type="button" onClick={this.search}>Buscar</button>
                        </form>

                        <button type="button" class="btn btn-info" onClick={this.logout}>Cerrar sesion</button>




                    </div>

                </nav>
                <br />
                {this.state.Video.map(video =>
                    <center>

                        <div class="card text-center">

                            <div class="card-body">

                                <h5 class="card-title">{video.name}</h5>
                                <ReactPlayer
                                    url={video.url}
                                    className='react-player'
                                    width='90%'
                                    height='100%'

                                />
                               

                            </div>

                        </div>
                        <br />


                    </center>

                )}

            </div>

        );




    }
}

export default videoPlayer;
